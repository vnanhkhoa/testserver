"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const celebrate_1 = require("celebrate");
const wallpaper_controller_1 = __importDefault(require("./wallpaper.controller"));
const validation_middleware_1 = __importDefault(require("../../middlewares/validation.middleware"));
class WallpaperRoute {
    constructor() {
        this.router = express_1.Router();
        this.wallpaperController = new wallpaper_controller_1.default();
        this.initializeRoutes();
    }
    initializeRoutes() {
        this.router.get('/wallpapers', validation_middleware_1.default({
            query: {
                category: celebrate_1.Joi.string().required(),
            }
        }), this.wallpaperController.getWallpapers);
    }
}
exports.default = WallpaperRoute;
//# sourceMappingURL=wallpaper.route.js.map