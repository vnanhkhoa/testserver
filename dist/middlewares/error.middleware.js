"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function errorMiddleware(err, req, res, next) {
    return res.error(err);
}
exports.default = errorMiddleware;
//# sourceMappingURL=error.middleware.js.map