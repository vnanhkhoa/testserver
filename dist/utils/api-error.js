"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
class ApiError extends Error {
    constructor(message, status = http_status_1.default.INTERNAL_SERVER_ERROR) {
        super(message);
        this.status = status;
    }
}
exports.default = ApiError;
//# sourceMappingURL=api-error.js.map