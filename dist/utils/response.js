"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const celebrate_1 = require("celebrate");
const http_status_1 = __importDefault(require("http-status"));
const api_error_1 = __importDefault(require("./api-error"));
function default_1(req, res, next) {
    res.success = function (status = 200, data) {
        return this.status(status).json(data).end();
    };
    res.error = function (err) {
        if (process.env.NODE_ENV === 'development')
            console.error(err);
        let responseError = err;
        if (celebrate_1.isCelebrate(err)) {
            responseError = err;
            responseError.status = http_status_1.default.BAD_REQUEST;
        }
        else if (!(err instanceof api_error_1.default)) {
            responseError = new api_error_1.default(err.message, err.status);
        }
        return this.status(responseError.status).json({ message: responseError.message, status: responseError.status }).end();
    };
    return next();
}
exports.default = default_1;
//# sourceMappingURL=response.js.map