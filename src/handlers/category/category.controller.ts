import httpStatus = require('http-status');
import { NextFunction } from 'express';
import IRequestWithUser from '../../interfaces/requestWithUser.interface';
import IResponse from '../../interfaces/responseWithHelper.interface';
import fs from 'fs';

export default class CategoryController {
    public getCategories = async (req: IRequestWithUser, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const categories = [
                {
                    id: "1",
                    name: "Khoa",
                    img: "https://image.com/img1.png"
                },
                {
                    id: "2",
                    name: "Khoa1",
                    img: "https://image.com/img2.png"
                }
            ]

            return res.success(httpStatus.OK, categories);
        } catch (err) {
            return next(err);
        }
    };

    public download = async (req: IRequestWithUser, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const file = `./video.mp4`;
            return res.sendFile(file);
        } catch (err) {
            return next(err);
        }
    };
}
