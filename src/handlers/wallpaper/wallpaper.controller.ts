import httpStatus = require('http-status');
import { NextFunction } from 'express';
import { get, set } from 'lodash';
import IRequestWithUser from '../../interfaces/requestWithUser.interface';
import IResponse from '../../interfaces/responseWithHelper.interface';

export default class WallpaperController {
    public getWallpapers = async (req: IRequestWithUser, res: IResponse, next: NextFunction): Promise<any> => {
        try {
            const wallpapers = [
                {
                    thumb: "https://image.com/thumb1.png",
                    img: "https://image.com/img1.png"
                }
            ]

            return res.success(httpStatus.OK, wallpapers);

        } catch (err) {
            return next(err);
        }
    };
}
