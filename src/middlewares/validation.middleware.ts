import { RequestHandler } from 'express';
import { celebrate } from 'celebrate';

function validationMiddleware<T>(schema: any): RequestHandler {
    return celebrate(schema);
}

export default validationMiddleware;
