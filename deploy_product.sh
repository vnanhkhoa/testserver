pm2 stop all
# git pull
git pull origin main;
# build
npm install;
npm run build;

# install
pm2 start dist/index.js